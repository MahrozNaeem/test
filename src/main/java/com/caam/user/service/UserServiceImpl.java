package com.caam.user.service;

import com.caam.commons.exceptionhandler.CaamUserAlreadyExistException;
import com.caam.commons.exceptionhandler.UserNotFoundException;
import com.caam.commons.user.dto.ActivationDto;
import com.caam.commons.user.dto.LoginDto;
import com.caam.commons.user.dto.UserDto;
import com.caam.commons.user.dto.UserInfoDto;
import com.caam.user.domain.CaamUser;
import com.caam.user.repo.UserRepository;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.Instant;
import java.util.Optional;
import java.util.concurrent.CompletableFuture;

import static com.caam.commons.user.UserUtills.*;
import static com.caam.user.constants.UserMessages.*;

@Service
public class UserServiceImpl implements UserService {
    /**
     * Logger for this class
     */
    private static final Logger logger = LogManager.getLogger(UserServiceImpl.class.getName());

    @Autowired
    private UserRepository userRepository;

    @Autowired
            private com.caam.user.client.notification.NotificationClient notificationClient;

    boolean jobStatus = false;
    UserInfoDto userInfoDto = null;

    @Override
    public UserInfoDto saveUser(UserDto userDto) throws Exception {
        Optional<CaamUser> caamUser = userRepository.findByloginString(userDto.getLoginString());
        if (caamUser.isPresent())
            throw new CaamUserAlreadyExistException(LOGIN_STRING_ALREADY_EXIST);

        CaamUser newUser = getMapper().map(userDto, CaamUser.class);
        newUser.setCreatedDate(Instant.now());
        newUser.setPassword(getEncodedPassword(userDto.getPassword()));
        newUser.setActivated(false);
        if (userDto.getLoginString().contains("@"))
            newUser.setActivationToken(generateToken());
        else
            newUser.setActivationOtp(generateOTP());
        newUser = userRepository.save(newUser);
        logger.info("Output:" + newUser.toString());
//sending email to user for activation		
        if (newUser.getId() != 0 && userDto.getLoginString().contains("@")) {
            userDto.setActivationToken(newUser.getActivationToken());
            CompletableFuture.runAsync(() -> {
                logger.info("Inside Future:");
                notificationClient.sendActivationEmail(userDto);
            });
        } else if (newUser.getId() != 0 && !userDto.getLoginString().contains("@")) {
            logger.info("Inside Mobile Future1");
        }
        UserInfoDto userInfoDto = null;
        try {
            userInfoDto = getMapper().map(newUser,UserInfoDto.class);
        }catch (Exception ex){
            throw new Exception("User info not mapped successfully.");
        }

        return userInfoDto;
    }

    @Override
    public UserInfoDto login(LoginDto loginDto) throws UserNotFoundException {
        Optional<CaamUser> rtnUserByLoginString = userRepository.findByloginString(loginDto.getLoginString());
        if (!rtnUserByLoginString.isPresent())
            throw new UserNotFoundException(USER_NOT_FOUND);
        if (getEncoder().matches(loginDto.getPassword(), rtnUserByLoginString.get().getPassword())) {
            return getMapper().map(rtnUserByLoginString.get(),UserInfoDto.class);
        } else {
            throw new UserNotFoundException(INVALID_PASSWORD);
        }
    }

    @Override
    public boolean activateUser(ActivationDto activationDto) throws UserNotFoundException {
        logger.info(activationDto.getActivationToken());
        Optional<CaamUser> caamUser = userRepository.findByTokenOrOtp(activationDto.getActivationToken(),
                activationDto.getActivationOtp());
        jobStatus = false;
        if (!caamUser.isPresent())
            throw new UserNotFoundException(USER_NOT_FOUND);
        if (caamUser.get().getId() != 0) {
            logger.info("Here inside if" + jobStatus);
            caamUser.get().setActivated(true);
            caamUser.get().setActivationToken("");
            caamUser.get().setActivationOtp(1);
            userRepository.save(caamUser.get());
            jobStatus = true;
        }
        return jobStatus;

    }

    @Override
    public boolean forgotPassword(String loginString) throws UserNotFoundException {

        Optional<CaamUser> rtnUserByLoginString = userRepository.findByloginString(loginString);
        if (!rtnUserByLoginString.isPresent())
            throw new UserNotFoundException(USER_NOT_FOUND);

        try {
            rtnUserByLoginString.get().setForgotPasswordToken(generateToken());
            userRepository.save(rtnUserByLoginString.get());
            userInfoDto = getMapper().map(rtnUserByLoginString.get(), UserInfoDto.class);
        } catch (NullPointerException e) {
            throw new UserNotFoundException(USER_NOT_FOUND);
        }

        if (userInfoDto.getId() != 0 && userInfoDto.getLoginString().contains("@")) {
            CompletableFuture.runAsync(() -> {
                notificationClient.sendForgotPasswordEmail(userInfoDto);
                jobStatus = true;
            });
        } else if (userInfoDto.getId() != 0 && !userInfoDto.getLoginString().contains("@")) {
            logger.info("Inside Mobile Future1");
        }

        return jobStatus;
    }

    @Override
    public UserInfoDto getCaamUser(String loginString) throws UserNotFoundException {
        CaamUser caamUser = this.userRepository.getByLoginString(loginString);
        if (caamUser != null)
            return new ModelMapper().map(caamUser, UserInfoDto.class);
        return null;
    }

    @Override
    public UserInfoDto getCaamUserById(long userId) throws UserNotFoundException {
        CaamUser caamUser = this.userRepository.getById(userId);
        if (caamUser != null)
            return new ModelMapper().map(caamUser, UserInfoDto.class);
        return null;
    }


}
