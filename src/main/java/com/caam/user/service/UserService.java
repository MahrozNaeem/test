package com.caam.user.service;

import com.caam.commons.exceptionhandler.UserNotFoundException;
import com.caam.commons.user.dto.ActivationDto;
import com.caam.commons.user.dto.LoginDto;
import com.caam.commons.user.dto.UserDto;
import com.caam.commons.user.dto.UserInfoDto;
import org.springframework.stereotype.Service;


@Service
public interface UserService {

    UserInfoDto saveUser(UserDto userDto) throws Exception;

    UserInfoDto login(LoginDto loginDto) throws UserNotFoundException;

    boolean activateUser(ActivationDto activationDto) throws UserNotFoundException;

    boolean forgotPassword(String loginString) throws UserNotFoundException;

    UserInfoDto getCaamUser(String loginString) throws UserNotFoundException;

    UserInfoDto getCaamUserById(long loginUserId) throws UserNotFoundException;

}
