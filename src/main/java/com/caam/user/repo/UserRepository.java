package com.caam.user.repo;

import com.caam.user.domain.CaamUser;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface UserRepository extends CrudRepository<CaamUser, Integer> {

    public Optional<CaamUser> findByloginString(String login);

    CaamUser findByloginStringAndActivatedTrueAndVerifiedTrue(String login);

    //    @Query("select new ModelMapper(cu.userId,cu.name,cu.activationToken,cu.loginString,cu.forgotPasswordToken,cu.activated,cu.verified) from CaamUser cu")
    CaamUser getByLoginString(String login);

    CaamUser getById(long loginUserId);

    @Query(value = "select * from caamuser ", nativeQuery = true)
    public List<CaamUser> getAll();

    @Query(value = "select  * from caamuser where (activation_token=?1 OR activation_otp=?2) AND activation_otp!=1", nativeQuery = true)
    public Optional<CaamUser> findByTokenOrOtp(String activationtoken, int otp);


}
