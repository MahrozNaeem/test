package com.caam.user.constants;

public class UserMessages {
	public static final String LOGIN_STRING_ALREADY_EXIST = "Mobile/Email Already Exist!!";
	public static final String USER_REGISTER_SUCCESS = "User Created Successfully";
	public static final String GENERAL_EXCEPTION_MSG = "This Service currently not available due to following reason";
	public static final String USER_REGISTER_FAILURE = "Something went wrong!! User not saved";
	public static final String USER_LOGIN_SUCCESS = "User Sign In Successfully";
	public static final String INVALID_PASSWORD = "Invalid password!!Please Try Again";
	public static final String INVALID_LOGIN_STRING = "Invalid Email/Mobile !!";
	public static final String USER_ACTIVATION_SUCCESS = "User Activate Successfully";
	public static final String USER_NOT_FOUND = "User Not Found!!";

}
