package com.caam.user.domain;

import java.time.Instant;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

@Entity
@Table(name = "caamuser")
public class CaamUser extends BaseDomain {

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE)
    @Column(name = "id")
    private Long id;

    @Column(name = "name")
//	@JsonProperty(value="name$1")
    @NotNull
//	@Pattern(regexp = Constants.userName_REGEX)
    @Size(min = 1, max = 50)
    private String name;

    @NotNull
//	@Pattern(regexp = Constants.userName_REGEX)
//	@Size(min = 1, max = 50)
    @Column(name = "password")
    private String password;

    @Column(name = "login_String")
    @Size(min = 1, max = 50)
    private String loginString;

    @Column(name = "created_By")
    private String createdBy;

    @Column(name = "created_Date")
    private Instant createdDate;

    @Column(name = "last_Modified_By")
    private String lastModifiedBy;

    @Column(name = "last_Modified_Date")
    private Instant lastModifiedDate;

    @NotNull
    @Column(nullable = false)
    private boolean activated;
    @NotNull
    @Column(nullable = false)
    private boolean verified;

    @Column(name = "activation_Token")
    private String activationToken;

    @Column(name = "activation_Otp")
    private int activationOtp;

    @Column(name = "forgot_Password_Token")
    private String forgotPasswordToken;


    public String getForgotPasswordToken() {
        return forgotPasswordToken;
    }

    public void setForgotPasswordToken(String forgotPasswordToken) {
        this.forgotPasswordToken = forgotPasswordToken;
    }

    public int getActivationOtp() {
        return activationOtp;
    }

    public void setActivationOtp(int activationOtp) {
        this.activationOtp = activationOtp;
    }

    public String getActivationToken() {
        return activationToken;
    }

    public void setActivationToken(String activationToken) {
        this.activationToken = activationToken;
    }

    public boolean isActivated() {
        return activated;
    }

    public void setActivated(boolean activated) {
        this.activated = activated;
    }

    public String getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }

    public Instant getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(Instant createdDate) {
        this.createdDate = createdDate;
    }

    public String getLastModifiedBy() {
        return lastModifiedBy;
    }

    public void setLastModifiedBy(String lastModifiedBy) {
        this.lastModifiedBy = lastModifiedBy;
    }

    public Instant getLastModifiedDate() {
        return lastModifiedDate;
    }

    public void setLastModifiedDate(Instant lastModifiedDate) {
        this.lastModifiedDate = lastModifiedDate;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }


    @Override
    public String toString() {
        return "CaamUser [id=" + id + ", name=" + name + ", password=" + password + ", loginString="
                + loginString + ", createdBy=" + createdBy + ", createdDate=" + createdDate + ", lastModifiedBy="
                + lastModifiedBy + ", lastModifiedDate=" + lastModifiedDate + ", activated=" + activated + "]";
    }

    public String getLoginString() {
        return loginString;
    }

    public void setLoginString(String loginString) {
        this.loginString = loginString;
    }

    public CaamUser() {
    }

    public boolean isVerified() {
        return verified;
    }

    public void setVerified(boolean verified) {
        this.verified = verified;
    }
}
