package com.caam.user.client.notification;

import com.caam.commons.user.dto.UserDto;
import com.caam.commons.user.dto.UserInfoDto;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;

@FeignClient(name = "CAAM-NOTIFICATION-SERVICE")
public interface NotificationClient {

	@PostMapping("/api/activationEmail")
	ResponseEntity sendActivationEmail(@RequestBody UserDto dto);

	@PostMapping("/api/forgotPassword") 
	ResponseEntity sendForgotPasswordEmail(@RequestBody UserInfoDto dto);
}
