package com.caam.user.controller;

import com.caam.commons.exceptionhandler.UserNotFoundException;
import com.caam.commons.response.utills.ResponseUtill;
import com.caam.commons.user.constants.CaamResponseCodes;
import com.caam.commons.user.dto.ActivationDto;
import com.caam.commons.user.dto.LoginDto;
import com.caam.commons.user.dto.UserDto;
import com.caam.commons.user.dto.UserInfoDto;
import com.caam.user.service.UserService;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import static com.caam.user.constants.UserMessages.*;

@RestController
@RequestMapping("/api/user/")
@CrossOrigin(origins = "*")
public class UserController {

	private static final Logger logger = LogManager.getLogger(UserController.class.getName());

	@Autowired
	private UserService userService;

	@Autowired
	private ResponseUtill responseUtill;

	@PostMapping("save")
	public @ResponseBody ResponseEntity saveUser(@RequestBody UserDto userDto) throws Exception {
		logger.info("REST request for saveuser : {}", userDto);
		return responseUtill.getApiResponse(CaamResponseCodes.CREATED_CODE, USER_REGISTER_SUCCESS, userService.saveUser(userDto));
	}

	@PostMapping("signin")
	public ResponseEntity login(@RequestBody LoginDto loginDto) throws UserNotFoundException {
		return responseUtill.getApiResponse(CaamResponseCodes.SUCCESS_CODE, USER_LOGIN_SUCCESS, userService.login(loginDto));
	}

	@PostMapping("activate")
	public ResponseEntity login(@RequestBody ActivationDto activationDto) throws UserNotFoundException {
		System.out.print("Here");
		userService.activateUser((activationDto));
		return responseUtill.getApiResponse(CaamResponseCodes.SUCCESS_CODE, USER_ACTIVATION_SUCCESS, null);
	}

	@PostMapping("forgot/{loginString}")
	public ResponseEntity forgotPassword(@PathVariable(value = "loginString") String loginString)
			throws UserNotFoundException {
		userService.forgotPassword(loginString);
		return responseUtill.getApiResponse(CaamResponseCodes.SUCCESS_CODE, "Email Sent", null);
	}

	@PostMapping("get")
	public @ResponseBody
	UserInfoDto getCaamUserByLoginString(@RequestBody LoginDto loginDto)
			throws UserNotFoundException {
		return userService.getCaamUser(loginDto.getLoginString());

	}

	@PostMapping("get/loginString")
	public @ResponseBody UserInfoDto getCaamUserInfoByLoginString(@RequestBody String loginString)
			throws UserNotFoundException {
		return userService.getCaamUser(loginString);
	}

	@PostMapping("get/id")
	public @ResponseBody UserInfoDto getUserInfoByUserId(@RequestBody long id) throws UserNotFoundException {
		return userService.getCaamUserById(id);
	} 

}
