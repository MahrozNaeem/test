package com.caam.commons.base.domain;

import javax.persistence.Transient;

public class BaseDomain {
	@Transient
	private int responseCode;
	@Transient
	private String responseMsg;

	public int getResponseCode() {
		return responseCode;
	}

	public void setResponseCode(int responseCode) {
		this.responseCode = responseCode;
	}

	public String getResponseMsg() {
		return responseMsg;
	}

	public void setResponseMsg(String responseMsg) {
		this.responseMsg = responseMsg;
	}

	public BaseDomain() {

	}

}
