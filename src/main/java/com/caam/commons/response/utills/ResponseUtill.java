package com.caam.commons.response.utills;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;

@Component
public class ResponseUtill {
//	@Autowired
	CaamResponse caamResponse=new CaamResponse();
//	@Autowired
	CaamResponseHeader responseHeader=new CaamResponseHeader();
//	@Autowired
	CaamResponseBody responseBody=new CaamResponseBody();

	public CaamResponse apiResponse(int code, String msg, Object data) {
		responseHeader.setCode(code);
		responseHeader.setMsg(msg);
		responseBody.setData(data);
		caamResponse.setResponseHeader(responseHeader);
		caamResponse.setResponseBody(responseBody);
		return caamResponse;
	}// end of engine Response

	public ResponseEntity getApiResponse(int code, String msg, Object data) {
		if (code == 200)
			return new ResponseEntity(apiResponse(code, msg, data), HttpStatus.OK);
		else if (code == 201)
			return new ResponseEntity(apiResponse(code, msg, data), HttpStatus.CREATED);
		else if (code == 404)
			return new ResponseEntity(apiResponse(code, msg, data), HttpStatus.NOT_FOUND);
		else if (code == 500)
			return new ResponseEntity(apiResponse(code, msg, data), HttpStatus.INTERNAL_SERVER_ERROR);
		else if (code == 400)
			return new ResponseEntity(apiResponse(code, msg, data), HttpStatus.BAD_REQUEST);
		else if (code == 401)
			return new ResponseEntity(apiResponse(code, msg, data), HttpStatus.UNAUTHORIZED);
		else if (code == 415)
			return new ResponseEntity(apiResponse(code, msg, data), HttpStatus.UNSUPPORTED_MEDIA_TYPE);
		else
			return new ResponseEntity(apiResponse(code, msg, data), HttpStatus.BAD_REQUEST);
	}

}
