package com.caam.commons.response.utills;

import java.util.List;

import org.springframework.stereotype.Component;

@Component
public class CaamResponseBody {
    private Object data;

    public CaamResponseBody() {
    }

    public Object getData() {
        return data;
    }

    public void setData(Object data) {
        this.data = data;
    }
}
