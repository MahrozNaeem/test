package com.caam.commons.response.utills;

import java.io.Serializable;

import org.springframework.stereotype.Component;

import com.caam.commons.response.utills.CaamResponseBody;
import com.caam.commons.response.utills.CaamResponseHeader;

@Component
public class CaamResponse implements Serializable {

	private static final long serialVersionUID = 1L;
	CaamResponseHeader responseHeader;
	CaamResponseBody responseBody;

	public CaamResponseHeader getResponseHeader() {
		return responseHeader;
	}

	public void setResponseHeader(CaamResponseHeader responseHeader) {
		this.responseHeader = responseHeader;
	}

	public CaamResponseBody getResponseBody() {
		return responseBody;
	}

	public void setResponseBody(CaamResponseBody responseBody) {
		this.responseBody = responseBody;
	}

	public CaamResponse() {
	}

}
