package com.caam.commons.exceptionhandler;

public class UserNotFoundException extends Exception {
	public UserNotFoundException(String exception) {
		super(exception);
	}
}
