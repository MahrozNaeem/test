package com.caam.commons.exceptionhandler;

import static com.caam.commons.user.constants.CaamResponseCodes.INTERNAL_SERVER_ERROR_CODE;
import static com.caam.commons.user.constants.CaamResponseCodes.NOT_FOUND_CODE;
import static com.caam.commons.user.constants.CaamResponseCodes.SUCCESS_CODE;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import com.caam.commons.response.utills.ResponseUtill;

@ControllerAdvice
@RestController
public class CustomizedResponseEntityExceptionHandler extends ResponseEntityExceptionHandler {

	@Autowired
	ResponseUtill responseUtill;

	@ExceptionHandler(UserNotFoundException.class)
	public final ResponseEntity handleUserNotFoundException(UserNotFoundException ex, WebRequest request) {
		return responseUtill.getApiResponse(NOT_FOUND_CODE, ex.getMessage(), null);
	}

	@ExceptionHandler(CaamUserAlreadyExistException.class)
	public final ResponseEntity handleUserAlreadyExistException(CaamUserAlreadyExistException ex, WebRequest request) {
		return responseUtill.getApiResponse(SUCCESS_CODE, ex.getMessage(), null);
	}

	@ExceptionHandler(CaamServiceNotAvailableException.class)
	public final ResponseEntity handleServiceNotAvailableException(CaamServiceNotAvailableException ex,
			WebRequest request) {
		return responseUtill.getApiResponse(INTERNAL_SERVER_ERROR_CODE, ex.getMessage(), null);
	}

	@ExceptionHandler(Exception.class)
	public final ResponseEntity handleAllExceptions(Exception ex) {
		return responseUtill.getApiResponse(INTERNAL_SERVER_ERROR_CODE, ex.getMessage(), null);
	}
}