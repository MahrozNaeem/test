package com.caam.commons.exceptionhandler;

public class CaamServiceNotAvailableException extends Exception {
	public CaamServiceNotAvailableException(String exception) {
		super(exception);
	}
}
