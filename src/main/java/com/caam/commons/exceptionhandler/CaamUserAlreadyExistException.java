package com.caam.commons.exceptionhandler;

public class CaamUserAlreadyExistException  extends Exception{

	public CaamUserAlreadyExistException(String exception) {
		super(exception);
	}

}
