package com.caam.commons.user;

import org.apache.commons.lang.RandomStringUtils;
import org.modelmapper.ModelMapper;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Component;
import org.springframework.util.DigestUtils;


@Component
public class UserUtills {


	static BCryptPasswordEncoder encoder;

	public static ModelMapper getMapper() {
		return new ModelMapper();
	}

	public static String getEncodedPassword(String password) {
		if (encoder == null)
			encoder = new BCryptPasswordEncoder();
		return encoder.encode(password);
	}

	public static BCryptPasswordEncoder getEncoder() {
		return new BCryptPasswordEncoder();
	}

	public static String generateToken() {
		return DigestUtils.md5DigestAsHex(RandomStringUtils.random(8).toString().getBytes());
	}

	public static int generateOTP() {
		try {
			return Integer.parseInt(RandomStringUtils.randomNumeric(4));
		} catch (Exception e) {
			return 0;
		}
	}

}