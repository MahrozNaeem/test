package com.caam.commons.user.constants;

public class CaamResponseCodes {

	public static final int SUCCESS_CODE = 200;
	public static final int CREATED_CODE = 201;
	public static final int NOT_FOUND_CODE =404 ;
	public static final int BAD_REQUEST_CODE =400 ;
	public static final int INTERNAL_SERVER_ERROR_CODE =500 ;
	public static final int UNAUTHORIZED_CODE =401 ;
	

	
	
}
