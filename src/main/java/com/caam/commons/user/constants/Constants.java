package com.caam.commons.user.constants;

public class Constants {
	 public static final String userName_REGEX = "^[_.@A-Za-z0-9-]*$";
	 public static final String GENERAL_EXCEPTION_MSG = "This Service currently not available due to following reason";
}
