package com.caam.commons.user.dto;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;

import com.caam.commons.user.constants.Constants;

public class UserDto {

	private int id;

	@NotBlank
	@Pattern(regexp = Constants.userName_REGEX)
	@Size(min = 1, max = 50)
	private String name;

//	@NotBlank
//	@Pattern(regexp = Constants.userName_REGEX)
//	@Size(min = 1, max = 50)
	private String password;

	private String activationToken;
	private String activationOtp;

	@Size(min = 1, max = 50)
	private String loginString;
	
	private String forgotPasswordToken;

	public String getActivationOtp() {
		return activationOtp;
	}

	public void setActivationOtp(String activationOtp) {
		this.activationOtp = activationOtp;
	}

	public String getForgotPasswordToken() {
		return forgotPasswordToken;
	}

	public void setForgotPasswordToken(String forgotPasswordToken) {
		this.forgotPasswordToken = forgotPasswordToken;
	}

	public UserDto() {

	}

	public String getActivationToken() {
		return activationToken;
	}

	public void setActivationToken(String activationToken) {
		this.activationToken = activationToken;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getLoginString() {
		return loginString;
	}

	public void setLoginString(String loginString) {
		this.loginString = loginString;
	}

	@Override
	public String toString() {
		return "UserDto [id=" + id + ", name=" + name + ", password=" + password + ", loginString="
				+ loginString + "]";
	}

}
