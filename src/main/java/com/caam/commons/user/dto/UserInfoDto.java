package com.caam.commons.user.dto;

public class UserInfoDto {

	private long id;
	private String name;
	private String activationToken;
	private String loginString;
	private String forgotPasswordToken;
	private boolean activated;
	private boolean verified;

	public long getId() {
		return id;
	}

    public UserInfoDto(long id, String name, String activationToken, String loginString, String forgotPasswordToken, boolean activated, boolean verified) {
        this.id = id;
        this.name = name;
        this.activationToken = activationToken;
        this.loginString = loginString;
        this.forgotPasswordToken = forgotPasswordToken;
        this.activated = activated;
        this.verified = verified;
    }

    public void setId(long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getActivationToken() {
		return activationToken;
	}

	public void setActivationToken(String activationToken) {
		this.activationToken = activationToken;
	}

	public String getLoginString() {
		return loginString;
	}

	public void setLoginString(String loginString) {
		this.loginString = loginString;
	}

	public String getForgotPasswordToken() {
		return forgotPasswordToken;
	}

	public void setForgotPasswordToken(String forgotPasswordToken) {
		this.forgotPasswordToken = forgotPasswordToken;
	}

	public UserInfoDto() {

	}

	public boolean isActivated() {
		return activated;
	}

	public void setActivated(boolean activated) {
		this.activated = activated;
	}

	public boolean isVerified() {
		return verified;
	}

	public void setVerified(boolean verified) {
		this.verified = verified;
	}
}
