FROM java:8
ADD target/caam-user-service.jar  caam-user-service.jar
EXPOSE 8082
ENTRYPOINT ["java", "-jar", "caam-user-service.jar"]